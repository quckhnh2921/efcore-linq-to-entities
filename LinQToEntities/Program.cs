﻿// khởi tạo đối tượng context đại diện cho 1 database
var context = new AppDbContext();
//var names = context.Persons.Select(x => new { x.Name, x.Id}).ToList();

//foreach (var name in names)
//{
//    Console.WriteLine(name);
//}

var nameAfter1992 = context.Persons.Where(x => x.DOB.Year > 1993).Select(x => x.Name).ToList();

foreach (var name in nameAfter1992)
{
    Console.WriteLine(name);
}

// làm tương tự với những từ khóa LinQ đã học và bật SQL profiler lên xem câu truy vấn được generate như thế nào
//orderby
//groupby
//aggregate function
//first, firstOrDefault, last, LastOrDefault, Single, SingleOrDefault, paging,
