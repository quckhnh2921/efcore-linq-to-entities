﻿// khởi tạo đối tượng context đại diện cho 1 database
using Newtonsoft.Json;

var context = new AppDbContext();

// tạo danh sách Product sẽ thêm vào database
//var s = new StreamReader("MOCK_DATA.json");
//var jr = new JsonTextReader(s);
//var js = new JsonSerializer();
//var obj = js.Deserialize<List<Product>>(jr);
//// đánh dấu là thêm vào database
//context.Products.AddRange(obj);

//// lưu mọi sự thay đổi
//context.SaveChanges();
//var number = context.Products.Count();
//Console.WriteLine($"number of record is: {number}");

//Lấy danh sách tất cả các sản phẩm.
context.Products.ToList();
//Lấy danh sách tên của tất cả các sản phẩm.
var listName = context.Products.Select(x => x.Name).ToList();
//Lấy danh sách các sản phẩm có giá lớn hơn 50 đồng.
var productPrice = context.Products.Where(p => p.Price > 50).ToList();
//Lấy danh sách các sản phẩm có số lượng tồn kho ít hơn 10.
var productStock = context.Products.Where(p=>p.StockQuantity < 10).ToList();
//Lấy danh sách tên của các sản phẩm có giá từ 100 đến 500 đồng.
var productPriceInRange = context.Products.Where(p => p.Price >= 100 && p.Price <= 500).ToList();
//Lấy danh sách các sản phẩm được tạo ra trong vòng 7 ngày gần đây.
TimeSpan sevenDay = TimeSpan.FromDays(7);
DateTime date = DateTime.Now - sevenDay;
var product7DayAgo = context.Products.Select(p => new { p.Name, p.CreatedAt, p.Price }).Where(p => p.CreatedAt >= date).ToList();
foreach (var item in product7DayAgo)
{
    Console.WriteLine(item.Name+ item.CreatedAt);
}
//Lấy danh sách các sản phẩm không còn tồn tại (không hoạt động).
var inActiveProduct = context.Products.Where(p => p.IsActive == false).ToList();
//Lấy tổng số lượng tồn kho của tất cả sản phẩm.
var totalStock = context.Products.Select(p => p.StockQuantity).Sum();
Console.WriteLine(totalStock);
//Lấy tên của sản phẩm và ngày tạo của sản phẩm có giá lớn hơn 200 đồng.
var nameProduct = context.Products.Select(p => new { p.Name, p.CreatedAt, p.Price }).Where(p => p.Price > 200).ToList();
foreach(var product in nameProduct)
{
    Console.WriteLine(product.Name + product.Price);
}
//Lấy danh sách các sản phẩm được sắp xếp theo giá giảm dần.
var nameProductDesc = context.Products.Select(p => new {p.Name, p.Price}).OrderByDescending(p => p.Price).ToList();

//Order By:

//Lấy danh sách các sản phẩm được sắp xếp theo tên sản phẩm tăng dần.
var orderByNameAsc = context.Products.Select(p => new { p.Name }).OrderBy(p => p.Name).ToList();
foreach (var item in orderByNameAsc)
{
    Console.WriteLine(item.Name);
}
//Lấy danh sách các sản phẩm được sắp xếp theo giá giảm dần.
var orderByPriceDesc = context.Products.Select(p => new { p.Price }).OrderByDescending(p => p.Price).ToList();
//Lấy danh sách các sản phẩm được sắp xếp theo ngày tạo mới nhất đến cũ nhất.
var oderByDateDesc = context.Products.Select(p => new {p.CreatedAt}).OrderByDescending(p => p.CreatedAt).ToList();

//Group By:

//Lấy danh sách các sản phẩm được nhóm theo số lượng tồn kho. (Mỗi nhóm hiển thị số lượng và tên các sản phẩm trong nhóm đó)
var groupByStock = context.Products.Select(p => new { p.Name, p.Price, p.StockQuantity }).GroupBy(p => p.StockQuantity).ToList();
foreach (var item in groupByStock)
{
    Console.WriteLine(item.Key);
    foreach (var product in item)
    {
        Console.WriteLine(product.Name + product.Price + product.StockQuantity);
    }
}
//Lấy danh sách các sản phẩm được nhóm theo khoảng giá (0-100, 101-200, v.v.). (Mỗi nhóm hiển thị khoảng giá và số lượng sản phẩm trong nhóm đó)
string GetRange(decimal Price)
{
    if (Price >= 0 && Price <= 100)
    {
        return "0-100";
    }
    else if (Price >= 101 && Price <= 200)
    {
        return "101-200";
    }
    else if (Price >= 201 && Price <= 300)
    {
        return "201-300";
    }
    else if (Price >= 301 && Price <= 400)
    {
        return "301-400";
    }
    else if (Price >= 401 && Price <= 500)
    {
        return "401-500";
    }
    else if (Price >= 501 && Price <= 600)
    {
        return "501-600";
    }
    else if (Price >= 601 && Price <= 700)
    {
        return "601-700";
    }
    else if (Price >= 701 && Price <= 800)
    {
        return "701-800";
    }
    else if (Price >= 801 && Price <= 900)
    {
        return "801-900";
    }
    return "901-1000";
}
var productList = context.Products.ToList();
var groupByPrice = productList.Select(p => new { p.Name, p.Price }).OrderBy(p => p.Price).GroupBy(p => GetRange(p.Price)).ToList();
foreach (var item in groupByPrice)
{
    Console.WriteLine(item.Key);
    foreach (var product in item)
    {
        Console.WriteLine(product.Name + product.Price);
    }
}

//First, FirstOrDefault, Last, LastOrDefault, Single, SingleOrDefault:

//Lấy thông tin chi tiết của sản phẩm đầu tiên trong danh sách.
var firstProduct = context.Products.Select(p => new { p.Name, p.Price, p.CreatedAt, p.StockQuantity }).First();
//Lấy thông tin chi tiết của sản phẩm có giá cuối cùng.
var lastProduct = context.Products.Select(p => new { p.Name, p.Price, p.CreatedAt, p.StockQuantity }).Last();
//Lấy thông tin chi tiết của sản phẩm có tên là "iPhone" (nếu có).
var firstOrDefault = context.Products.Select(p => new {p.Name, p.Price, p.CreatedAt, p.StockQuantity }).FirstOrDefault();
var lastOrDefault = context.Products.Select(p => new { p.Name, p.Price, p.CreatedAt, p.StockQuantity }).LastOrDefault();
//Lấy thông tin chi tiết của sản phẩm có ID là 5 (nếu có).
var singleOrDefault = context.Products.Select(p => new { p.Name, p.Price, p.CreatedAt, p.StockQuantity }).SingleOrDefault();
//Lấy thông tin chi tiết của sản phẩm có giá là 100 (nếu có).
var priceSingleOrDefault = context.Products.Select(p => new { p.Name, p.Price, p.CreatedAt, p.StockQuantity }).Where(p => p.Price == 100).SingleOrDefault();

//Paging:

//Lấy danh sách 10 sản phẩm đầu tiên trong danh sách.
var paging10 = context.Products.Select(p => new { p.Name, p.Price, p.CreatedAt, p.StockQuantity }).Take(10).ToList();
//Lấy danh sách 20 sản phẩm từ vị trí thứ 21 trong danh sách.
var paging20 = context.Products.Select(p => new { p.Name, p.Price, p.CreatedAt, p.StockQuantity }).Skip(20).Take(10).ToList();
