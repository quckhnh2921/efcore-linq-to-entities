﻿using Microsoft.EntityFrameworkCore;

// khởi tạo đối tượng context đại diện cho 1 database
var context = new AppDbContext();

// tạo danh sách person sẽ thêm vào database
var personData = new List<Person>
{
    new Person
    {
        Name = "A",
    },
    new Person
    {
        Name = "B",
    }
};

// đánh dấu là thêm vào database
context.Persons.AddRange(personData);

// lưu mọi sự thay đổi
var number = context.SaveChanges();

// number là số record được lưu thành công vào database
Console.WriteLine($"number of entity saved: {number}");

Console.WriteLine("get all the person in the memory");
// lấy toàn bộ person trong database ra
var persons = await context.Persons.ToListAsync();

foreach (var person in persons)
{
    Console.WriteLine($"{person.Id}  {person.Name}");
}

public class AppDbContext : DbContext
{
    public DbSet<Person> Persons { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseInMemoryDatabase("testDatabase");
    }
}

public class Person
{
    public int Id { get; set; }
    public string Name { get; set; }
}
